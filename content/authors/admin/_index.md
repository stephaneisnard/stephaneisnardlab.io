---
# Display name
name: Stéphane ISNARD

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Professeur Certifié <br> Ingénieur CNAM

# Organizations/Affiliations
organizations:
- name: Université Toulouse 2
  url: "//www.univ-tlse2.fr"
- name: IPST CNAM
  url: "http://www.ipst-cnam.fr"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests: []

education:
  courses:
  - year: 2008
    course: Certifications ITILv2 foundation et IPRC
    institution: I'FORM
  - year: 2007
    course: Ingénieur informatique
    institution: IPST-CNAM
  - year: 2003
    course: DEST informatique
    institution: IPST-CNAM
  - year: 1999
    course: Niveau maîtrise de physique
    institution: Université Paul Sabatier
  - year: 1998
    course: Licence de physique
    institution: Université Paul Sabatier
  - year: 1997
    course: Mathématiques spéciales
    institution: Lycée Post-Bac Saliège
  - year: 1996
    course: Mathématiques supérieures
    institution: Lycée Post-Bac Saliège


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/GeorgeCushen
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/gcushen
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed neque elit, tristique placerat feugiat ac, facilisis vitae arcu. Proin eget egestas augue. Praesent ut sem nec arcu pellentesque aliquet. Duis dapibus diam vel metus tempus vulputate.
